﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CentroControlerMobile : MonoBehaviour
{

    public float speed = 7;
    public float boostspeed = 17;
    public float jump = 50;
    
    public CharacterController charactercontroler;

    void Start()
    {
        
        charactercontroler = GetComponent<CharacterController>();

    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene("Menu");
            Menu.main = true;
            Menu.know = false;
            Menu.start = false;
            Menu.settings = false;
            Menu.startsure = false;
        }

        Vector3 vector3 = new Vector3(0 + speed, 0, 0);
        charactercontroler.Move(vector3 * Time.deltaTime);
        if (Input.GetKey(KeyCode.Mouse0))
        {
            if (Input.mousePosition.y < Screen.height / 2)
            {
                Vector3 move = new Vector3(0, -jump, 0);
                charactercontroler.Move(move * Time.deltaTime);
            }
            else if (Input.mousePosition.y > Screen.height / 2)
            {
                Vector3 move = new Vector3(0, +jump, 0);
                charactercontroler.Move(move * Time.deltaTime);
            }
        }
            
    }
}

