﻿using UnityEngine;
using System.Collections;

public class CentroControl : MonoBehaviour {

    public float speed = 7;
    public float boostspeed = 17;
    public float jump = 50;
    
    public CharacterController charactercontroler;

    void Start ()
    {
        charactercontroler = GetComponent<CharacterController>();
        
    }

	void Update ()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            boostmove();
            if (Input.GetKey(KeyCode.S))
            {

                Vector3 vector3 = new Vector3(0, -jump, 0);
                charactercontroler.Move(vector3 * Time.deltaTime);
            }
            else if (Input.GetKey(KeyCode.W))
            {
                Vector3 vector3 = new Vector3(0, +jump, 0);
                charactercontroler.Move(vector3 * Time.deltaTime);
            }
        }
        else
        {
            move();
            if (Input.GetKey(KeyCode.S))
            {

                Vector3 vector3 = new Vector3(0, -jump, 0);
                charactercontroler.Move(vector3 * Time.deltaTime);

            }
            else if (Input.GetKey(KeyCode.W))
            {
                Vector3 vector3 = new Vector3(0, +jump, 0);
                charactercontroler.Move(vector3 * Time.deltaTime);
            }
        }
	
	}

    private void move()
    {
        Vector3 vector3 = new Vector3(0+speed,0,0);
        Debug.Log(speed);
        charactercontroler.Move(vector3 * Time.deltaTime);
    }

    private void boostmove()
    {
        float walk = speed + boostspeed;
        Vector3 vector3 = new Vector3(0 + walk, 0, 0);
        Debug.Log(walk);
        charactercontroler.Move(vector3 * Time.deltaTime);
    }
}
