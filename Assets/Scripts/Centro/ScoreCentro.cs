﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreCentro : MonoBehaviour {

    public static bool dead = false;
    public float startScore;
    public Text Score_UIText;
    public Text Score_DeadText;
	
	void Update ()
    {
        score();
    }

    public void score()
    {
        if (dead == true)
        {
            Debug.Log("DEAD");
            int amount = (int)startScore;
            string amountText = amount.ToString();
            string scores = "Your score is " + amountText;
            Score_DeadText.text = scores;
        }
        else if (startScore >= 0)
        {
            startScore += Time.deltaTime;
            int amount = (int)startScore;
            string amountText = amount.ToString();
            string scores = "Scores " + amountText;
            Score_UIText.text = scores;
        }
    }
}
