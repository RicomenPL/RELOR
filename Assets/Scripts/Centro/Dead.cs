﻿using UnityEngine;
using System.Collections;

public class Dead : MonoBehaviour {

    public Canvas DeadPanel;
    public Canvas GUIPanel;

    void Start()
    {
        DeadPanel = DeadPanel.GetComponent<Canvas>();
        DeadPanel.enabled = false;
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Wals")
        {
            GUIPanel.enabled = false;
            DeadPanel.enabled = true;
            ScoreCentro.dead = true;
            Time.timeScale = 0;
            Debug.Log("PLS");
        }
    }
}

