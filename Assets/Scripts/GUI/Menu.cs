﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    public Canvas mainMenu;
    public Button playButton;
    public Button quitButton;
    public Button settingsButton;

    public Canvas settingsMenu;
    public Button aboutButton;
    public Button backButton;

    public Canvas StartsureMenu;
    public Button SureyesButton;
    public Button SurenoButton;
    public Button knowButton;

    public Canvas startMenu;
    public Button yesButton;
    public Button noButton;

    public Canvas knowMenu;

    public Canvas LoadBar;


    public static bool main = false;
    public static bool settings = false;
    public static bool startsure = false;
    public static bool start = true;
    public static bool know = false;
    public static bool load = false;



    void Start()
    {
        mainMenu = mainMenu.GetComponent<Canvas>();
        playButton = playButton.GetComponent<Button>();
        quitButton = quitButton.GetComponent<Button>();
        settingsButton = settingsButton.GetComponent<Button>();

        settingsMenu = settingsMenu.GetComponent<Canvas>();
        aboutButton = aboutButton.GetComponent<Button>();
        backButton = backButton.GetComponent<Button>();

        StartsureMenu = StartsureMenu.GetComponent<Canvas>();
        SureyesButton = SureyesButton.GetComponent<Button>();
        SurenoButton = SurenoButton.GetComponent<Button>();
        knowButton = knowButton.GetComponent<Button>();

        startMenu = startMenu.GetComponent<Canvas>();
        yesButton = yesButton.GetComponent<Button>();
        noButton = noButton.GetComponent<Button>();

        knowMenu = knowMenu.GetComponent<Canvas>();

        LoadBar = LoadBar.GetComponent<Canvas>();


        mainMenu.enabled = main;
        settingsMenu.enabled = settings;
        StartsureMenu.enabled = startsure;
        startMenu.enabled = start;
        knowMenu.enabled = know;
        LoadBar.enabled = load;
    }

    public void Yes()
    {
        startMenu.enabled = false;
        StartsureMenu.enabled = true;
    }

    public void SureYes()
    {
        StartsureMenu.enabled = false;
        LoadBar.enabled = true;
    }

    public void Know()
    {
        knowMenu.enabled = true;
        StartsureMenu.enabled = false;
    }
    

    public void Quit()
    {
        Application.Quit();
    }

    public void Settings()
    {
        mainMenu.enabled = false;
        settingsMenu.enabled = true;
    }

    public void Play()
    {
        Time.timeScale = 1;
        ScoreCentro.dead = false;
        SceneManager.LoadScene("TestScene");
    }

    public void BackMainMenu()
    {
        mainMenu.enabled = true;
        settingsMenu.enabled = false;
    }
}
