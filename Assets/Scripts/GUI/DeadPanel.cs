﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DeadPanel : MonoBehaviour {


    public void MainMenu()
    {
        SceneManager.LoadScene("Menu");
        Menu.main = true;
        Menu.know = false;
        Menu.start = false;
        Menu.settings = false;
        Menu.startsure = false;

    }

    public void Retry()
    {
        Time.timeScale = 1;
        ScoreCentro.dead = false;
        SceneManager.LoadScene("TestScene");
    }
}
