﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadingBar : MonoBehaviour {

    Image LoadBar;
    public float more;
    public float gravitation;
    public Text GraviText;
    public Text XDSmile;
    public Text SmileText;
    public Text LoadingText;
    public Canvas mainMenu;
    public Canvas Bar;

	void Start ()
    {
        LoadBar = GetComponent<Image>();
        LoadBar.fillAmount = 0f;

        GraviText = GraviText.GetComponent<Text>();
        XDSmile = XDSmile.GetComponent<Text>();
        SmileText = SmileText.GetComponent<Text>();
        LoadingText = LoadingText.GetComponent<Text>();

        mainMenu = mainMenu.GetComponent<Canvas>();
        Bar = Bar.GetComponent<Canvas>();
	}

	void Update ()
    {
        Debug.Log(Input.acceleration.x);

        if (Input.acceleration.x > 0.85)
        {
            LoadBar.fillAmount += gravitation * Time.deltaTime;
            XDSmile.enabled = true;
            GraviText.enabled = true;
            SmileText.enabled = true;
            LoadingText.enabled = false;

        }
        else
        {
            LoadBar.fillAmount += more * Time.deltaTime;
            XDSmile.enabled = false;
            GraviText.enabled = false;
            SmileText.enabled = false;
            LoadingText.enabled = true;
        }

        if (LoadBar.fillAmount == 1)
        {
            mainMenu.enabled = true;
            Bar.enabled = false;
            
        }


    }
}
